Element.prototype.tab = function (config) {
				this.default = Object.assign({
					tabActiveClass: 'active',
					boxShowClass: 'show',
					boxHideClass: 'hide',
				}, config);

				function setClick(self, tab) {
					tab.addEventListener("click", function() {
						[].forEach.call(this.parentElement.getElementsByTagName(this.tagName), function (el) {
							el.classList.remove(self.default.tabActiveClass);
							document.getElementById(el.dataset.tabtarget).classList.add(self.default.boxHideClass);
							document.getElementById(el.dataset.tabtarget).classList.remove(self.default.boxShowClass);
						});

			   			this.classList.add(self.default.tabActiveClass);
			   			document.getElementById(this.dataset.tabtarget).classList.add(self.default.boxShowClass);
			   			document.getElementById(this.dataset.tabtarget).classList.remove(self.default.boxHideClass);
			   		});
				}

				function init(self) {
					var listTab = self.dataset.tablist;
					[].forEach.call(document.getElementById(listTab).querySelectorAll('[data-tabtarget]'), function (tab) {
						setClick(self, tab);
					});
				}

				init(this);
			}
//Tab One
			document.getElementById('tab1').tab({
				tabActiveClass: 'active',
				boxShowClass: 'show',
				boxHideClass: 'hide',
			});

//Tab Two
			document.getElementById('tab2').tab({
				tabActiveClass: 'active',
				boxShowClass: 'show',
				boxHideClass: 'hide',
			});
			
//Tab Six
			document.getElementById('tab6').tab({
				tabActiveClass: 'active',
				boxShowClass: 'show',
				boxHideClass: 'hide',
			});
			
//Tab Exit
			document.getElementById('exit').tab({
				tabActiveClass: 'active',
				boxShowClass: 'show',
				boxHideClass: 'hide',
			});